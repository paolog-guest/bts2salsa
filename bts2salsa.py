#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# script to keep the salsa.debian.org issue tracker aligned with the debian BTS
# see: https://salsa.debian.org/paolog-guest/bts2salsa
#
# Copyright (C) 2018 Paolo Greppi <paolo.greppi@libpf.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import gitlab
import re

dryrun = False


def update_issue_from_bts(issue, bts_id, bts_severity, bts_title):
    title = '%d: [%s] %s' % (bts_id, bts_severity, bts_title)
    if not dryrun:
        project = git.projects.get(issue.project_id, lazy=True)
        editable_issue = project.issues.get(issue.iid, lazy=True)
        editable_issue.title = title
        editable_issue.save()
    print('  updated issue #%d' % issue.iid)


def delete_issue(issue):
    if not dryrun:
        project = git.projects.get(issue.project_id, lazy=True)
        editable_issue = project.issues.get(issue.iid, lazy=True)
        editable_issue.delete()
    print('  deleted issue #%d' % issue.iid)


def create_issue_from_bts(bts_id, bts_severity, bts_title):
    description = 'https://bugs.debian.org/%d' % bts_id
    title = '%d: [%s] %s' % (bts_id, bts_severity, bts_title)
    if not dryrun:
        project = git.projects.get(project_id, lazy=True)
        project.issues.create({'title': title, 'description': description})
    print('  created new issue for BTS bug #%d' % bts_id)


with open('token', 'r') as token_file:
    token = token_file.read()
git = gitlab.Gitlab("https://salsa.debian.org/", private_token=token)
project_id = 18891  # doxygen

all_issues = git.issues.list(as_list=False)
# filter project-specific and BTS-related issues
issues = []
for i in all_issues:
    if i.project_id == project_id:
        if re.match("^\\d*: .*$", i.title) is not None:
            issues.append(i)
# data structure:
#   [{'id': 2475, 'iid': 1, 'project_id': 18829, 'title': '886120: [serious] makes ctpp2 randomly FTBFS, syntax errors, hides problems', 'description': 'https://bugs.debian.org/886120', 'state': 'opened', 'created_at': '2018-03-16T06:42:09.534Z', 'updated_at': '2018-03-16T06:42:09.534Z', 'labels': [], 'milestone': None, 'author': {'id': 2024, 'name': 'Paolo Greppi', 'username': 'paolog-guest', 'state': 'active', 'avatar_url': 'https://salsa.debian.org/uploads/-/system/user/avatar/2024/avatar.png', 'web_url': 'https://salsa.debian.org/paolog-guest'}, 'assignee': None, 'user_notes_count': 0, 'upvotes': 0, 'downvotes': 0, 'due_date': None, 'confidential': False, 'web_url': 'https://salsa.debian.org/paolog-guest/doxygen/issues/1', 'subscribed': True}, ... ]

with open('bugs2.json', 'r') as bugs_file:
    bts_bugs = json.loads(bugs_file.read())

# data structure:
#   [{"id": 890285, "severity": "normal", "title": "doxygen: Please drop LLVM build-dependency on powerpcspe"}, ... ]

closed = 0
updated = 0
for i in issues:
    found = 0
    print('now looking at issue #%d' % i.iid)
    for bts_bug in bts_bugs:
        if int(i.title[:6]) == bts_bug['id']:
            found = bts_bug['id']
            print('  BTS bug %s is still present' % bts_bug['id'])
            # 3. update issue titles
            update_issue_from_bts(i, bts_bug['id'], bts_bug['severity'], bts_bug['title'])
            updated += 1
            break
    if found == 0:
        # 1. close open issues with no matching BTS bug
        delete_issue(i)
        closed += 1

created = 0
for bts_bug in bts_bugs:
    found = 0
    print('now looking at BTS bug #%d' % bts_bug['id'])
    for i in issues:
        if int(i.title[:6]) == bts_bug['id']:
            found = i.iid
            print('  issue #%d already present' % i.iid)
            break
    if found == 0:
        # 2. create new issues for new bts_bugs
        create_issue_from_bts(bts_bug['id'], bts_bug['severity'], bts_bug['title'])
        created += 1

print('closed: %d issues' % closed)
print('created: %d issues' % created)
print('updated: %d issues' % updated)
